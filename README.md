# hamberfim.github.io

### Sample Work:

[html-css Prototype Proposal](https://hamberfim.github.io/Kate_Isaacs/)

[Graphic design, mobile design & information architecture](https://hamberfim.github.io/Alcune_Opere_Grafiche/)

[Adobe Portfolio](https://adhamlin.myportfolio.com/)

[Github Webpage](https://hamberfim.github.io/)

[CSS Grid Experiment-Basic Hero](https://hamberfim.github.io/Simple_Web_Grid_Layouts/simple_hero/simple_hero.html)

[CSS Grid Experiment-wrap N stack](https://hamberfim.github.io/Simple_Web_Grid_Layouts/wrap_n_stack/index.html)

[CSS Grid Experiment-8 column](https://hamberfim.github.io/Simple_Web_Grid_Layouts/cust_materialize/index.html)

[CSS Grid Experiment-old school top nav](https://hamberfim.github.io/Simple_Web_Grid_Layouts/simple_hortz/index_hrzNav.html)

[CSS Grid Experiment-old school side nav](https://hamberfim.github.io/Simple_Web_Grid_Layouts/simple_sidebar/index_sbNav.html)


### Notice July 9 2018
Out of necessity projects are going to be spread across Github, Gitlab and Bitbucket.

### Notice June 12 2018:
Having issues at Gitlab again today. Been unable to access projects for several hours.

### Notice June 10 2018:
Began testing on GitLab. See me directly if you want access. Otherwise everything else is on the in house server.

### Notice March 2018:
All of the main projects created prior March 2018 have been removed from GitHub and either placed on the in house server or are remove since they have been delivered. If you need access to the archives we had here you will need to contact me directly.

If there is outstanding balance they will be required to pay off the balance or make other arrangements to recieve any additional deliverables. This includes any support on theses older projects since all contractual agreements have been completed.

### Notice Nov 2017:
Many of the main projects will be housed here on Github rather than the in house server since I need remote access and the IT Security Rules do not allow me access from outside the network. January and Febuary I will be on site and we will migrate all the projects back to the in house server.
